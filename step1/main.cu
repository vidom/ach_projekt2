/*
 * Architektura procesoru (ACH 2016)
 * Projekt c. 2 (cuda)
 * Login: xvidom00
 */

#include <sys/time.h>
#include <cstdio>
#include <cmath>

#include "nbody.h"

int main(int argc, char **argv)
{
    FILE *fp;
    struct timeval t1, t2;
    int N;
    float dt;
    int steps;
    int thr_blc;
    int blc_gr;
    int Npad;
    cudaError_t ret;

    // parametry
    if (argc != 7)
    {
        printf("Usage: nbody <N> <dt> <steps> <thr/blc> <input> <output>\n");
        exit(1);
    }
    N = atoi(argv[1]);
    dt = atof(argv[2]);
    steps = atoi(argv[3]);
    thr_blc = atoi(argv[4]);
    blc_gr = 1 + ((N - 1) / thr_blc);
    Npad = blc_gr * thr_blc;

    printf("N: %d\n", N);
    printf("dt: %f\n", dt);
    printf("steps: %d\n", steps);
    printf("threads/block: %d\n", thr_blc);
    printf("blocks/grid: %d\n", blc_gr);
    printf("N with padding: %d\n", Npad);

    // alokace pameti na CPU
    t_particles particles_cpu;
    // TODO ZDE DOPLNTE ALOKACI PAMETI NA CPU
    if (cudaHostAlloc(&particles_cpu.pos_x, Npad * sizeof(float), cudaHostAllocMapped) != cudaSuccess) {
        printf("cudaHostAlloc() particles_cpu.pos_x error!\n");
        exit(1);
    }
    if (cudaHostAlloc(&particles_cpu.pos_y, Npad * sizeof(float), cudaHostAllocMapped) != cudaSuccess) {
        printf("cudaHostAlloc() particles_cpu.pos_y error!\n");
        exit(1);
    }
    if (cudaHostAlloc(&particles_cpu.pos_z, Npad * sizeof(float), cudaHostAllocMapped) != cudaSuccess) {
        printf("cudaHostAlloc() particles_cpu.pos_z error!\n");
        exit(1);
    }
    if (cudaHostAlloc(&particles_cpu.vel_x, Npad * sizeof(float), cudaHostAllocMapped) != cudaSuccess) {
        printf("cudaHostAlloc() particles_cpu.vel_x error!\n");
        exit(1);
    }
    if (cudaHostAlloc(&particles_cpu.vel_y, Npad * sizeof(float), cudaHostAllocMapped) != cudaSuccess) {
        printf("cudaHostAlloc() particles_cpu.vel_y error!\n");
        exit(1);
    }
    if (cudaHostAlloc(&particles_cpu.vel_z, Npad * sizeof(float), cudaHostAllocMapped) != cudaSuccess) {
        printf("cudaHostAlloc() particles_cpu.vel_z error!\n");
        exit(1);
    }
    if (cudaHostAlloc(&particles_cpu.weight, Npad * sizeof(float), cudaHostAllocMapped) != cudaSuccess) {
        printf("cudaHostAlloc() particles_cpu.weight error!\n");
        exit(1);
    }

    // nacteni castic ze souboru
    fp = fopen(argv[5], "r");
    if (fp == NULL)
    {
        printf("Can't open file %s!\n", argv[5]);
        exit(1);
    }
    particles_read(fp, particles_cpu, N);
    fclose(fp);

    t_particles particles_gpu[2];
    int in = 0;
    int out = 1;
    for (int i = 0; i < 2; i++)
    {
        // alokace pameti na GPU
        // TODO ZDE DOPLNTE ALOKACI PAMETI NA GPU
        if (cudaMalloc(&particles_gpu[i].pos_x, Npad * sizeof(float)) != cudaSuccess) {
            printf("cudaMalloc() particles_gpu[%d].pos_x error!\n", i);
            exit(1);
        }
        if (cudaMalloc(&particles_gpu[i].pos_y, Npad * sizeof(float)) != cudaSuccess) {
            printf("cudaMalloc() particles_gpu[%d].pos_y error!\n", i);
            exit(1);
        }
        if (cudaMalloc(&particles_gpu[i].pos_z, Npad * sizeof(float)) != cudaSuccess) {
            printf("cudaMalloc() particles_gpu[%d].pos_z error!\n", i);
            exit(1);
        }
        if (cudaMalloc(&particles_gpu[i].vel_x, Npad * sizeof(float)) != cudaSuccess) {
            printf("cudaMalloc() particles_gpu[%d].vel_x error!\n", i);
            exit(1);
        }
        if (cudaMalloc(&particles_gpu[i].vel_y, Npad * sizeof(float)) != cudaSuccess) {
            printf("cudaMalloc() particles_gpu[%d].vel_y error!\n", i);
            exit(1);
        }
        if (cudaMalloc(&particles_gpu[i].vel_z, Npad * sizeof(float)) != cudaSuccess) {
            printf("cudaMalloc() particles_gpu[%d].vel_z error!\n", i);
            exit(1);
        }
        if (cudaMalloc(&particles_gpu[i].weight, Npad * sizeof(float)) != cudaSuccess) {
            printf("cudaMalloc() particles_gpu[%d].weight error!\n", i);
            exit(1);
        }

        // kopirovani castic na GPU
        // TODO ZDE DOPLNTE KOPIROVANI DAT Z CPU NA GPU
        if (cudaMemcpy(particles_gpu[i].pos_x, particles_cpu.pos_x, Npad * sizeof(float), cudaMemcpyHostToDevice) != cudaSuccess) {
            printf("cudaMemcpy() particles_gpu[%d].pos_x error!\n", i);
            exit(1);
        }
        if (cudaMemcpy(particles_gpu[i].pos_y, particles_cpu.pos_y, Npad * sizeof(float), cudaMemcpyHostToDevice) != cudaSuccess) {
            printf("cudaMemcpy() particles_gpu[%d].pos_y error!\n", i);
            exit(1);
        }
        if (cudaMemcpy(particles_gpu[i].pos_z, particles_cpu.pos_z, Npad * sizeof(float), cudaMemcpyHostToDevice) != cudaSuccess) {
            printf("cudaMemcpy() particles_gpu[%d].pos_z error!\n", i);
            exit(1);
        }
        if (cudaMemcpy(particles_gpu[i].vel_x, particles_cpu.vel_x, Npad * sizeof(float), cudaMemcpyHostToDevice) != cudaSuccess) {
            printf("cudaMemcpy() particles_gpu[%d].vel_x error!\n", i);
            exit(1);
        }
        if (cudaMemcpy(particles_gpu[i].vel_y, particles_cpu.vel_y, Npad * sizeof(float), cudaMemcpyHostToDevice) != cudaSuccess) {
            printf("cudaMemcpy() particles_gpu[%d].vel_y error!\n", i);
            exit(1);
        }
        if (cudaMemcpy(particles_gpu[i].vel_z, particles_cpu.vel_z, Npad * sizeof(float), cudaMemcpyHostToDevice) != cudaSuccess) {
            printf("cudaMemcpy() particles_gpu[%d].vel_z error!\n", i);
            exit(1);
        }
        if (cudaMemcpy(particles_gpu[i].weight, particles_cpu.weight, Npad * sizeof(float), cudaMemcpyHostToDevice) != cudaSuccess) {
            printf("cudaMemcpy() particles_gpu[%d].weight error!\n", i);
            exit(1);
        }
    }

    // vypocet
    gettimeofday(&t1, 0);
    for (int s = 0; s < steps; s++)
    {
        // TODO ZDE DOPLNTE SPUSTENI KERNELU
        particles_simulate<<<blc_gr, thr_blc, thr_blc * sizeof(float4)>>> (particles_gpu[in], particles_gpu[out], N, dt);

        // Vymenit vstupne a vystupne pole.
        in = in + out;
        out = in - out;
        in = in - out;
    }
    // TODO ZDE DOPLNTE SYNCHRONIZACI
    cudaDeviceSynchronize();
    gettimeofday(&t2, 0);

    // cas
    double t = (1000000.0 * (t2.tv_sec - t1.tv_sec) + t2.tv_usec - t1.tv_usec) / 1000000.0;
    printf("Time: %f s\n", t);

    // kopirovani castic zpet na CPU
    // TODO ZDE DOPLNTE KOPIROVANI DAT Z GPU NA CPU
    if ((ret = cudaMemcpy(particles_cpu.pos_x, particles_gpu[in].pos_x, N * sizeof(float), cudaMemcpyDeviceToHost)) != cudaSuccess) {
        printf("cudaMemcpy() particles_cpu.pos_x error %d!\n", ret);
        exit(1);
    }
    if ((ret = cudaMemcpy(particles_cpu.pos_y, particles_gpu[in].pos_y, N * sizeof(float), cudaMemcpyDeviceToHost)) != cudaSuccess) {
        printf("cudaMemcpy() particles_cpu.pos_y error %d!\n", ret);
        exit(1);
    }
    if ((ret = cudaMemcpy(particles_cpu.pos_z, particles_gpu[in].pos_z, N * sizeof(float), cudaMemcpyDeviceToHost)) != cudaSuccess) {
        printf("cudaMemcpy() particles_cpu.pos_z error %d!\n", ret);
        exit(1);
    }
    if ((ret = cudaMemcpy(particles_cpu.vel_x, particles_gpu[in].vel_x, N * sizeof(float), cudaMemcpyDeviceToHost)) != cudaSuccess) {
        printf("cudaMemcpy() particles_cpu.vel_x error %d!\n", ret);
        exit(1);
    }
    if ((ret = cudaMemcpy(particles_cpu.vel_y, particles_gpu[in].vel_y, N * sizeof(float), cudaMemcpyDeviceToHost)) != cudaSuccess) {
        printf("cudaMemcpy() particles_cpu.vel_y error %d!\n", ret);
        exit(1);
    }
    if ((ret = cudaMemcpy(particles_cpu.vel_z, particles_gpu[in].vel_z, N * sizeof(float), cudaMemcpyDeviceToHost)) != cudaSuccess) {
        printf("cudaMemcpy() particles_cpu.vel_z error %d!\n", ret);
        exit(1);
    }
    if ((ret = cudaMemcpy(particles_cpu.weight, particles_gpu[in].weight, N * sizeof(float), cudaMemcpyDeviceToHost)) != cudaSuccess) {
        printf("cudaMemcpy() particles_cpu.weight error %d!\n", ret);
        exit(1);
    }

    // ulozeni castic do souboru
    fp = fopen(argv[6], "w");
    if (fp == NULL)
    {
        printf("Can't open file %s!\n", argv[6]);
        exit(1);
    }
    particles_write(fp, particles_cpu, N);
    fclose(fp);

    // Uvolnenie alokovanych pamatovych oblasti.
    if (particles_cpu.pos_x) cudaFreeHost(particles_cpu.pos_x);
    if (particles_cpu.pos_y) cudaFreeHost(particles_cpu.pos_y);
    if (particles_cpu.pos_z) cudaFreeHost(particles_cpu.pos_z);
    if (particles_cpu.vel_x) cudaFreeHost(particles_cpu.vel_x);
    if (particles_cpu.vel_y) cudaFreeHost(particles_cpu.vel_y);
    if (particles_cpu.vel_z) cudaFreeHost(particles_cpu.vel_z);
    if (particles_cpu.weight) cudaFreeHost(particles_cpu.weight);
    for (int i = 0; i < 2; i++) {
        if (particles_gpu[i].pos_x) cudaFree(particles_gpu[i].pos_x);
        if (particles_gpu[i].pos_y) cudaFree(particles_gpu[i].pos_y);
        if (particles_gpu[i].pos_z) cudaFree(particles_gpu[i].pos_z);
        if (particles_gpu[i].vel_x) cudaFree(particles_gpu[i].vel_x);
        if (particles_gpu[i].vel_y) cudaFree(particles_gpu[i].vel_y);
        if (particles_gpu[i].vel_z) cudaFree(particles_gpu[i].vel_z);
        if (particles_gpu[i].weight) cudaFree(particles_gpu[i].weight);
    }

    return 0;
}
