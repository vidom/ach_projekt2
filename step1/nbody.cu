/*
 * Architektura procesoru (ACH 2016)
 * Projekt c. 2 (cuda)
 * Login: xvidom00
 */

#include <cmath>
#include <cfloat>
#include "nbody.h"

#define POW2(x) ((x)*(x))
__global__ void particles_simulate(t_particles p_in, t_particles p_out, int N, float dt)
{
    // TODO ZDE DOPLNTE TELO KERNELU
    int j = threadIdx.x + blockDim.x * blockIdx.x;
    float Fx = 0.0f;
    float Fy = 0.0f;
    float Fz = 0.0f;
    float Gdt = G * dt;
    extern __shared__ float4 p[];
    int padsize = N / blockDim.x;
    int unpadsize = N % blockDim.x;

    for (int k = 0; k < padsize; k++)
    {
        p[threadIdx.x].x = p_in.pos_x[k * blockDim.x + threadIdx.x];
        p[threadIdx.x].y = p_in.pos_y[k * blockDim.x + threadIdx.x];
        p[threadIdx.x].z = p_in.pos_z[k * blockDim.x + threadIdx.x];
        p[threadIdx.x].w = p_in.weight[k * blockDim.x + threadIdx.x];
        __syncthreads();

        for (int i = 0; i < blockDim.x; i++)
        {
            float sum = POW2(p_in.pos_x[j] - p[i].x) +
                POW2(p_in.pos_y[j] - p[i].y) +
                POW2(p_in.pos_z[j] - p[i].z);
            float R3 = sum * sqrtf(sum) + FLT_EPSILON;
            Fx += (p[i].w * (p[i].x - p_in.pos_x[j])) / R3;
            Fy += (p[i].w * (p[i].y - p_in.pos_y[j])) / R3;
            Fz += (p[i].w * (p[i].z - p_in.pos_z[j])) / R3;
        }
        __syncthreads();
    }
    for (int k = 0; k < unpadsize; k++)
    {
        float sum = POW2(p_in.pos_x[j] - p_in.pos_x[padsize * blockDim.x + k]) +
            POW2(p_in.pos_y[j] - p_in.pos_y[padsize * blockDim.x + k]) +
            POW2(p_in.pos_z[j] - p_in.pos_z[padsize * blockDim.x + k]);
        float R3 = sum * sqrtf(sum) + FLT_EPSILON;
        Fx += (p_in.weight[padsize * blockDim.x + k] * (p_in.pos_x[padsize * blockDim.x + k] - p_in.pos_x[j])) / R3;
        Fy += (p_in.weight[padsize * blockDim.x + k] * (p_in.pos_y[padsize * blockDim.x + k] - p_in.pos_y[j])) / R3;
        Fz += (p_in.weight[padsize * blockDim.x + k] * (p_in.pos_z[padsize * blockDim.x + k] - p_in.pos_z[j])) / R3;
    }

    p_out.vel_x[j] = p_in.vel_x[j] + (Fx * Gdt);
    p_out.vel_y[j] = p_in.vel_y[j] + (Fy * Gdt);
    p_out.vel_z[j] = p_in.vel_z[j] + (Fz * Gdt);
    p_out.pos_x[j] = p_in.pos_x[j] + (p_out.vel_x[j] * dt);
    p_out.pos_y[j] = p_in.pos_y[j] + (p_out.vel_y[j] * dt);
    p_out.pos_z[j] = p_in.pos_z[j] + (p_out.vel_z[j] * dt);
}

void particles_read(FILE *fp, t_particles &p, int N)
{
    for (int i = 0; i < N; i++)
    {
        fscanf(fp, "%f %f %f %f %f %f %f \n",
            // TODO ZDE DOPLNTE NACTENI JEDNE CASTICE
            &p.pos_x[i], &p.pos_y[i], &p.pos_z[i],
            &p.vel_x[i], &p.vel_y[i], &p.vel_z[i],
            &p.weight[i]);
    }
}

void particles_write(FILE *fp, t_particles &p, int N)
{
    for (int i = 0; i < N; i++)
    {
        fprintf(fp, "%10.10f %10.10f %10.10f %10.10f %10.10f %10.10f %10.10f \n",
            // TODO ZDE DOPLNTE VYPSANI JEDNE CASTICE
            p.pos_x[i], p.pos_y[i], p.pos_z[i],
            p.vel_x[i], p.vel_y[i], p.vel_z[i],
            p.weight[i]);
    }
}
